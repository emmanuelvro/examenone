﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebUsuarios.Models;
using WebUsuarios.Models.DataBase;

namespace WebUsuarios.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ExamenOneCoreEntities entidad;

        public AccountController()
        {
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            try
            {

                if (this.Request.IsAuthenticated)
                {

                    return this.RedirectToLocal(returnUrl);
                }
            }
            catch (Exception ex)
            {

                Debug.Write(ex.Message);
            }

            return this.View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Login(string Usuario, string Password)
        {
            LoginStatus status = new LoginStatus();
            try
            {

                if (ModelState.IsValid)
                {
                    t_Usuarios detalleUsuario = new t_Usuarios();
                    using (entidad = new ExamenOneCoreEntities())
                    {
                        detalleUsuario = this.entidad.t_Usuarios.Where(x => x.Usuario == Usuario).FirstOrDefault();
                    }

                    if (detalleUsuario != null)
                    {
                        if (detalleUsuario.Password == Seguridad.Utilidades.Encrypt(Password.Trim(), true))
                        {
                            if (detalleUsuario.Status)
                            {
                                this.iniciarSession(detalleUsuario.Usuario, false);
                                status.Respuesta = true;
                                status.Mensaje = "Bienvenido: " + detalleUsuario.Usuario;
                                status.Url = "/";
                            }
                            else
                            {
                                status.Respuesta = false;
                                status.Mensaje = "El usuario esta desactivado.";
                                status.Url = "/";
                            }
                        }
                        else
                        {
                            status.Respuesta = false;
                            status.Mensaje = "La contraseña ingresada es incorrecta.";
                            status.Url = "/";
                        }
                        
                        
                    }
                    else
                    {

                        status.Respuesta = false;
                        status.Mensaje = "El usuario no existe.";
                        status.Url = "/";
                    }
                }
            }
            catch (Exception ex)
            {

                status.Respuesta = false;
                status.Mensaje = "Ocurrio un error al procesar la solicitud";
                status.Url = "/";
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult LogOff()
        {
            try
            {

                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;

                authenticationManager.SignOut();
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);

            }

            return this.RedirectToAction("Login", "Account");
        }


        private void iniciarSession(string username, bool isPersistent)
        {

            var claims = new List<Claim>();
            try
            {

                claims.Add(new Claim(ClaimTypes.Name, username));
                var claimIdenties = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;

                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, claimIdenties);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            try
            {
                if (Url.IsLocalUrl(returnUrl))
                {

                    return this.Redirect(returnUrl);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return this.RedirectToAction("Index", "Home");
        }


    }
}