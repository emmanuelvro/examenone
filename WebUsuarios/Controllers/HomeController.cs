﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUsuarios.Models;
using WebUsuarios.Models.DataBase;

namespace WebUsuarios.Controllers
{
    public class HomeController : Controller
    {
        private ExamenOneCoreEntities entidad;
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetUsuarios()
        {
           
            List<Usuario> usuarios = new List<Usuario>();
            using (entidad = new ExamenOneCoreEntities())
            {
                usuarios = entidad.t_Usuarios.Select(x => new Usuario
                {
                    Id = x.Id,
                    User = x.Usuario,
                    IdSexo = x.IdSexo,
                    Email = x.Email,
                    Sexo = x.t_cat_sexo.Sexo,
                    Estatus = x.Status
                }).ToList();
            }

            return Json(new { data = usuarios, recordsFiltered = usuarios.Count, recordsTotal = usuarios.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddUser(Usuario u)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if(u.Password != u.PasswordConfirm)
                        return Json("Las contraseñas no coinciden", JsonRequestBehavior.AllowGet);
                    t_Usuarios us = new t_Usuarios()
                    {
                        Email = u.Email,
                        Usuario = u.User,
                        IdSexo = u.IdSexo,
                        Password = Seguridad.Utilidades.Encrypt(u.Password,true),
                        Status = true,
                        FechaCreacion = DateTime.Now
                    };
                using (entidad = new ExamenOneCoreEntities())
                    {
                        entidad.t_Usuarios.Add(us);
                        entidad.SaveChanges();
                }
                }
                catch (Exception ex)
                {

                    return Json("Ocurrio un error en el proceso", JsonRequestBehavior.AllowGet);
                }
            }
            

            return Json("Se agrego correctamente", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult EditUser(Usuario u)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (u.Password != u.PasswordConfirm)
                        return Json("Las contraseñas no coinciden", JsonRequestBehavior.AllowGet);
                    t_Usuarios us = new t_Usuarios();
                    using (entidad = new ExamenOneCoreEntities())
                    {
                        us = entidad.t_Usuarios.Where(x => x.Id == u.Id).Select(x => x).FirstOrDefault();

                        if(us!= null)
                        {
                            us.IdSexo = u.IdSexo;
                            us.Password = u.Password;
                            us.Usuario = u.User;
                            us.Email = u.Email;
                            entidad.Entry(us).State = System.Data.Entity.EntityState.Modified ;
                            entidad.SaveChanges();
                        }
                        
                    }
                }
                catch (Exception ex)
                {

                    return Json("Ocurrio un error en el proceso", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Se actualizo correctamente", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public JsonResult DesactivaActiva(int id)
        {
            string mensaje = string.Empty;
            using (entidad = new ExamenOneCoreEntities())
            {
                var us = entidad.t_Usuarios.Where(x => x.Id == id).Select(x => x).FirstOrDefault();
                if(us != null)
                {
                    if (us.Status)
                    {
                        us.Status = false;
                        mensaje = "Se desactivo el usuario: " + us.Usuario;
                    }
                    else
                    {
                        us.Status = true;
                        mensaje = "Se activo el usuario: " + us.Usuario;
                    }

                    entidad.Entry(us).State = System.Data.Entity.EntityState.Modified;
                    entidad.SaveChanges();
                }
            }

            return Json(mensaje, JsonRequestBehavior.AllowGet);
        }
        
    }
}