﻿$(document).ready(function () {
	$("#Usuario").focus();
	$("#login-access").click(function () {
		if ($.trim($("#Usuario").val()) === "") {

			$("#Usuario").focus();
			$("#Usuario").css("border-color", "#800");
			msg("Error de Acceso", "Debes ingresar el usuario", "danger");

		} else if ($.trim($("#Password").val()) === "") {
			$("#Usuario").css("border-color", "");
			$("#Password").focus();
			$("#Password").css("border-color", "#800");
			msg("Error de Acceso", "Debes ingresar el password", "danger");
		} else {
			$(this).attr("disabled", "disabled");
			$("#Usuario").attr("disabled", "disabled");
			$("#Password").attr("disabled", "disabled");
			$("#login-loading").show();
			$("#Usuario").css("border-color", "");
			$("#Password").css("border-color", "");
			var $url = "/Account/Login";
			$.ajax({
				type: "POST",
				url: $url,
				data: {
					Usuario: $("#Usuario").val(),
					Password: $("#Password").val()
				},
				success: function (status) {

					if (status.Respuesta) {

						msg("Acceso Correcto", status.Mensaje, "success");

						setTimeout(function () {
							window.location = status.Url;
						}, 4000);

					} else {
						$("#login-loading").hide();
						$("#login-access").removeAttr("disabled");
						$("#Usuario").removeAttr("disabled");
						$("#Password").removeAttr("disabled");
						msg("Error", status.Mensaje, "danger");
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$("#login-loading").hide();
					$("#login-access").removeAttr("disabled");
					$("#Usuario").removeAttr("disabled");
					$("#Password").removeAttr("disabled");
					msg("Error", "Ocurrio un error al procesar la solicitud", "danger");
					console.log('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
					console.log('jqXHR:');
					console.log(jqXHR);
					console.log('textStatus:');
					console.log(textStatus);
					console.log('errorThrown:');
					console.log(errorThrown);
				}
			});
		}
		return false;

	});

});


function msg(titulo,mensaje, tipo) {
	$.notify({
		// options
		icon: 'fa fa-close',
		title: titulo,
		message: mensaje,
		
	}, {
			// settings
			element: 'body',
			position: null,
			type: tipo,
			allow_dismiss: true,
			newest_on_top: false,
			showProgressbar: false,
			placement: {
				from: "top",
				align: "center"
			},
			offset: 20,
			spacing: 10,
			z_index: 1031,
			delay: 5000,
			timer: 1000,
			mouse_over: null,
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			},
			onShow: null,
			onShown: null,
			onClose: null,
			onClosed: null,
			icon_type: 'class',
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
				'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
				'<span data-notify="icon"></span> ' +
				'<span data-notify="title">{1}</span> ' +
				'<span data-notify="message">{2}</span>' +
				'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				'</div>' +
				'</div>'
		});
}