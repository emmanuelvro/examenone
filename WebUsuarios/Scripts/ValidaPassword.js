﻿function validaSexo() {
	if ($('#IdSexo').val().trim() === '') {
		$('#resultSexo').addClass('text-danger');
		$('#resultSexo').html("");
		$('#resultSexo').html("Seleccione el sexo");
		return "Seleccione el sexo";

	} 
}

function isEmail(email) {
	if ($.trim(email) === '') {
		$('#resultEmail').addClass('text-danger');
		$('#resultEmail').html("");
		$('#resultEmail').html("Ingrese el email");
		return "Ingrese el email";
	}
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!regex.test(email)) {
		$('#resultEmail').addClass('text-danger');
		$('#resultEmail').html("");
		$('#resultEmail').html("Email invalido");
		return "Email invalido";
	}
	
}

function validaUsuario(usuario) {
	if ($.trim(usuario) === '') {
		$('#resultUsuario').addClass('text-danger');
		$('#resultUsuario').html("");
		$('#resultUsuario').html("Ingrese el usuario");
		return "Ingrese el usuario";
	}
	if (usuario.length < 7) {
		$('#resultUsuario').addClass('text-danger');
		$('#resultUsuario').html("");
		$('#resultUsuario').html("Longitud mínima 7 caracteres");
		return "Longitud mínima 7 caracteres";
	}
}

function validaPasswords(password,passwordConfirm) {
	if ($.trim(passwordConfirm) === '') {
		$('#resultConfirm').addClass('text-danger');
		$('#resultConfirm').html("");
		$('#resultConfirm').html("Debe confirmar el password");
		return "Debe confirmar el password";
	}
	if (password !== passwordConfirm) {
		$('#resultConfirm').addClass('text-danger');
		$('#resultConfirm').html("");
		$('#resultConfirm').html("Las contraseñas no coinciden");
		return "Las contraseñas no coinciden";
	}
}

function validaPassword(password) {
	if ($.trim(password) === '') {
		$('#result').addClass('text-danger');
		$('#result').html("");
		$('#result').html("Ingrese el password");
		return "Ingrese el password";
	}
	
	var strength = 0
	if (password.length < 10) {
		$('#result').removeClass();
		$('#result').addClass('text-danger')
		$('#result').html("");
		$('#result').html("La longitud debe ser de al menos 10 caracteres");
		return 'La longitud debe ser de al menos 10 caracteres'
	}
	var mensaje = "";
	if (password.length > 9) strength += 1

	if (!password.match(/([A-Z])/)) {

		$('#result').addClass('text-danger');
		$('#result').html("");
		$('#result').html("La contraseña debe contener al menos una mayúscula");
		return "La contraseña debe contener al menos una mayúscula";

	} else {
		strength += 1
	}

	if (!password.match(/([a-z])/)) {

		$('#result').addClass('text-danger');
		$('#result').html("");
		$('#result').html("La contraseña debe contener al menos una minúscula");
		return "La contraseña debe contener al menos una minúscula";

	} else {
		strength += 1
	}
	if (!password.match(/([0-9])/)) {

		$('#result').addClass('text-danger');
		$('#result').html("");
		$('#result').html("La contraseña debe contener al menos un número");
		return "La contraseña debe contener al menos un número";

	} else {
		strength += 1
	}

	if (!password.match(/(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ])/)) {
		
		$('#result').addClass('text-danger');
		$('#result').html("");
		$('#result').html("La contraseña debe contener al menos un caracter especial");
		return "La contraseña debe contener al menos un caracter especial";

	} else {
		strength += 1
	}
	
	/*
	if (strength < 2) {
		$('#result').removeClass()
		$('#result').addClass('text-danger')
		return 'Seguridad Baja'
	} else if (strength === 2) {
		$('#result').removeClass()
		$('#result').addClass('text-warning')
		return 'Seguridad Media'
	} else {
		$('#result').removeClass()
		$('#result').addClass('text-success')
		return 'Seguridad Alta'
	}
	*/
}


function validaSexoEdit() {
	if ($('#IdSexoEdit').val().trim() === '') {
		$('#resultSexoEdit').addClass('text-danger');
		$('#resultSexoEdit').html("");
		$('#resultSexoEdit').html("Seleccione el sexo");
		return "Seleccione el sexo";

	}
}

function isEmailEdit(email) {
	if ($.trim(email) === '') {
		$('#resultEmailEdit').addClass('text-danger');
		$('#resultEmailEdit').html("");
		$('#resultEmailEdit').html("Ingrese el email");
		return "Ingrese el email";
	}
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!regex.test(email)) {
		$('#resultEmailEdit').addClass('text-danger');
		$('#resultEmailEdit').html("");
		$('#resultEmailEdit').html("Email invalido");
		return "Email invalido";
	}

}

function validaUsuarioEdit(usuario) {
	if ($.trim(usuario) === '') {
		$('#resultUsuarioEdit').addClass('text-danger');
		$('#resultUsuarioEdit').html("");
		$('#resultUsuarioEdit').html("Ingrese el usuario");
		return "Ingrese el usuario";
	}
	if (usuario.length < 7) {
		$('#resultUsuarioEdit').addClass('text-danger');
		$('#resultUsuarioEdit').html("");
		$('#resultUsuarioEdit').html("Longitud mínima 7 caracteres");
		return "Longitud mínima 7 caracteres";
	}
}

function validaPasswordsEdit(password, passwordConfirm) {
	if ($.trim(passwordConfirm) === '') {
		$('#resultConfirmEdit').addClass('text-danger');
		$('#resultConfirmEdit').html("");
		$('#resultConfirmEdit').html("Debe confirmar el password");
		return "Debe confirmar el password";
	}
	if (password !== passwordConfirm) {
		$('#resultConfirmEdit').addClass('text-danger');
		$('#resultConfirmEdit').html("");
		$('#resultConfirmEdit').html("Las contraseñas no coinciden");
		return "Las contraseñas no coinciden";
	}
}

function validaPasswordEdit(password) {
	if ($.trim(password) === '') {
		$('#resultEdit').addClass('text-danger');
		$('#resultEdit').html("");
		$('#resultEdit').html("Ingrese el password");
		return "Ingrese el password";
	}

	var strength = 0
	if (password.length < 10) {
		$('#resultEdit').removeClass();
		$('#resultEdit').addClass('text-danger')
		$('#resultEdit').html("");
		$('#resultEdit').html("La longitud debe ser de al menos 10 caracteres");
		return 'La longitud debe ser de al menos 10 caracteres'
	}
	var mensaje = "";
	if (password.length > 9) strength += 1

	if (!password.match(/([A-Z])/)) {

		$('#resultEdit').addClass('text-danger');
		$('#resultEdit').html("");
		$('#resultEdit').html("La contraseña debe contener al menos una mayúscula");
		return "La contraseña debe contener al menos una mayúscula";

	} else {
		strength += 1
	}

	if (!password.match(/([a-z])/)) {

		$('#resultEdit').addClass('text-danger');
		$('#resultEdit').html("");
		$('#resultEdit').html("La contraseña debe contener al menos una minúscula");
		return "La contraseña debe contener al menos una minúscula";

	} else {
		strength += 1
	}
	if (!password.match(/([0-9])/)) {

		$('#resultEdit').addClass('text-danger');
		$('#resultEdit').html("");
		$('#resultEdit').html("La contraseña debe contener al menos un número");
		return "La contraseña debe contener al menos un número";

	} else {
		strength += 1
	}

	if (!password.match(/(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ])/)) {

		$('#resultEdit').addClass('text-danger');
		$('#resultEdit').html("");
		$('#resultEdit').html("La contraseña debe contener al menos un caracter especial");
		return "La contraseña debe contener al menos un caracter especial";

	} else {
		strength += 1
	}
}