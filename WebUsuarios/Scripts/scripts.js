﻿$(document).ready(function () {

	$('#IdSexoEdit').change(function () {

		$('#resultSexoEdit').html("");

	})
	$('#UsuarioEdit').keyup(function () {

		$('#resultUsuarioEdit').html("");
		validaUsuarioEdit($('#UsuarioEdit').val());

	})
	$('#PasswordEdit').keyup(function () {
		$('#resultEdit').html("");
		$('#resultEdit').html(validaPasswordEdit($('#PasswordEdit').val()))
	})

	$('#EmailEdit').keyup(function () {
		$('#resultEmailEdit').html("");
		$('#resultEmailEdit').html(isEmailEdit($('#EmailEdit').val()))
	});
	$('#PasswordConfirmEdit').keyup(function () {
		$('#resultConfirm').html("");
		$('#resultConfirm').html(validaPasswordsEdit($("#PasswordEdit").val(), $('#PasswordConfirmEdit').val()))
	})

	$('#IdSexo').change(function () {

		$('#resultSexo').html("");

	})
	$('#Usuario').keyup(function () {

		$('#resultUsuario').html("");
		validaUsuario($('#Usuario').val());

	})
	$('#Password').keyup(function () {
		$('#result').html("");
		$('#result').html(validaPassword($('#Password').val()))
	})

	$('#Email').keyup(function () {
		$('#resultEmail').html("");
		$('#resultEmail').html(isEmail($('#Email').val()))
	});
	$('#PasswordConfirm').keyup(function () {
		$('#resultConfirm').html("");
		$('#resultConfirm').html(validaPasswords($("#Password").val(), $('#PasswordConfirm').val()))
	})
	$("#AddUser").click(function () {
		$("#login-loading").show();
		validaUsuario($("#Usuario").val());
		isEmail($("#Email").val());
		validaPassword($("#Password").val());
		validaPasswords($("#Password").val(), $("#PasswordConfirm").val());
		validaSexo();

		if (validaUsuario($("#Usuario").val()) !== undefined) {
			console.log("Usuario");
			$("#login-loading").hide();
			return false;
		}

		else if (isEmail($("#Email").val()) !== undefined) {
			console.log("Email");
			$("#login-loading").hide();
			return false;
		}

		else if (validaPassword($("#Password").val()) !== undefined) {
			console.log("Password");
			$("#login-loading").hide();
			return false;
		}

		else if (validaPasswords($("#Password").val(), $("#PasswordConfirm").val()) !== undefined) {
			console.log("Confirma");
			$("#login-loading").hide();
			return false;
		}

		else if (validaSexo() !== undefined) {
			console.log("Sexo");
			$("#login-loading").hide();
			return false;
		}
		else {
			var values =
				{
					"User": $("#Usuario").val(),
					"Email": $("#Email").val(),
					"Password": $("#Password").val(),
					"PasswordConfirm": $("#PasswordConfirm").val(),
					"IdSexo": parseInt($("#IdSexo").val())
				};
			console.log(values);
			$.ajax({
				url: '/Home/AddUser',
				type: 'POST',
				data: values,
				success: function (result) {
					$("#login-loading").hide();
					msg("", result, "success");
					$("#Usuario").val('');
					$("#Email").val('');
					$("#Password").val('')
					$("#PasswordConfirm").val('')
					$('#IdSexo').prop('selectedIndex', 0);
					$('#ModalAlta').modal('hide');
				}
			});
		}


	});

	$("#EditUser").click(function () {
		$("#login-loading-edit").show();
		validaUsuarioEdit($("#UsuarioEdit").val());
		isEmailEdit($("#EmailEdit").val());
		validaPasswordEdit($("#PasswordEdit").val());
		validaPasswordsEdit($("#PasswordEdit").val(), $("#PasswordConfirmEdit").val());
		validaSexoEdit();

		if (validaUsuarioEdit($("#UsuarioEdit").val()) !== undefined) {
			console.log("Usuario");
			$("#login-loading-edit").hide();
			return false;
		}

		else if (isEmailEdit($("#EmailEdit").val()) !== undefined) {
			console.log("Email");
			$("#login-loading-edit").hide();
			return false;
		}

		else if (validaPasswordEdit($("#PasswordEdit").val()) !== undefined) {
			console.log("Password");
			$("#login-loading-edit").hide();
			return false;
		}

		else if (validaPasswordsEdit($("#PasswordEdit").val(), $("#PasswordConfirmEdit").val()) !== undefined) {
			console.log("Confirma");
			$("#login-loading-edit").hide();
			return false;
		}

		else if (validaSexoEdit() !== undefined) {
			console.log("Sexo");
			$("#login-loading-edit").hide();
			return false;
		}
		else {
			var values =
				{
					"Id": $("#IdUsuario").val(),
					"User": $("#UsuarioEdit").val(),
					"Email": $("#EmailEdit").val(),
					"Password": $("#PasswordEdit").val(),
					"PasswordConfirm": $("#PasswordConfirmEdit").val(),
					"IdSexo": parseInt($("#IdSexoEdit").val())
				};
			console.log(values);
			$.ajax({
				url: '/Home/EditUser',
				type: 'POST',
				data: values,
				success: function (result) {
					$("#login-loading-edit").hide();
					msg("", result, "success");
					$("#UsuarioEdit").val('');
					$("#EmailEdit").val('');
					$("#PasswordEdit").val('')
					$("#PasswordConfirmEdit").val('')
					$('#IdSexoEdit').prop('selectedIndex', 0);
					$('#ModalEdit').modal('hide');
				}
			});
		}


	});

});
$(document).on("click", ".modalLink", function () {
	var passedID = $(this).data('id');
	var passedUser = $(this).data('user');
	var passedEmail = $(this).data('email');
	var passedSexo = $(this).data('sexo');
	$(".modal-body .hiddenid").val(passedID);
	$(".modal-body #UsuarioEdit").val(passedUser);
	$(".modal-body #EmailEdit").val(passedEmail);

	$(".modal-body #IdSexoEdit").val(parseInt(passedSexo));
});
var tblUsuarios = $('#Usuarios').dataTable({

	ajax: {
		url: '/Home/GetUsuarios',
		liveAjax: true,
		interval: 1000
	},
	"iDisplayLength": 10,
	"aLengthMenu": [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
	"order": [[0, "asc"]],
	"oLanguage": {
		"sProcessing": "Cargando Transacciones",
		"sSearch": "Buscar: ",
		"oPaginate": {

			"sNext": "Siguiente",
			"sLast": "Última",
			"sFirst": "Primera",
			"sPrevious": "Anterior"
		},
		"sLengthMenu": "Ver _MENU_ registros",
		"sInfo": "Mostrando un total de _TOTAL_ ( _START_ a _END_ )",
		"sInfoFiltered": "(filtrado de _MAX_ de entradas totales)"
	},
	columns: [
		{
			data: "Id",
			className: "text-center",
			autowidth: true
		},
		{
			data: "User",
			className: "text-center",
			autowidth: true
		},
		{
			data: "Email",
			className: "text-center",
			autowidth: true
		},
		{
			data: "Sexo",
			className: "text-center",
			autowidth: true
		},
		{
			data: "Estatus",
			className: "text-center",
			autowidth: true,
			render: function (data, type, row) {
				if (data)
					return '<td aling="center">SI</td>';
				else
					return '<td aling="center">NO</td>';

			}
		},
		{
			data: "Accion",
			autowidth: true,
			render: function (data, type, row) {

				if (row.Estatus)
					return '<td aling="center"><a href="javascript:void(0);" onclick="DesactivaActiva(' + row.Id + ')"> <i class="fa fa-close"></i> Elimina</a>  <a href="/Home/EditUser' + row.Id + '" data-id="' + row.Id + '" data-user="' + row.User + '" data-email="' + row.Email + '" data-sexo="' + row.IdSexo + '" data-toggle="modal" data-target="#ModalEdit" class="modalLink"><i class="fa fa-edit"></i> Editar</a></td>';
				else
					return '<td aling="center"><a href="javascript:void(0);" onclick="DesactivaActiva(' + row.Id + ')"> <i class="fa fa-check"></i> Activar</a>  <a href="/Home/EditUser' + row.Id + '" data-id="' + row.Id + '" data-user="' + row.User + '" data-email="' + row.Email + '" data-sexo="' + row.IdSexo + '" data-toggle="modal" data-target="#ModalEdit" class="modalLink"><i class="fa fa-edit"></i> Editar</a></td>';
			}
		}

	],
	dom: 'Bfrtip',
	buttons: [
		{
			extend: 'excelHtml5'
		}

	]
});
$('#ModalEdit').on('hidden.bs.modal', function () {
	$('#resultSexoEdit').html("");
	$('#resultUsuarioEdit').html("");
	$('#resultEdit').html("");
	$('#resultEmailEdit').html("");
	$('#resultConfirmEdit').html("");
	$("#PasswordEdit").val('')
	$("#PasswordConfirmEdit").val('')
})

$('#ModalAlta').on('hidden.bs.modal', function () {
	$('#resultSexo').html("");
	$('#resultUsuario').html("");
	$('#result').html("");
	$('#resultEmail').html("");
	$('#resultConfirm').html("");
	$("#Usuario").val('');
	$("#Email").val('');
	$("#Password").val('')
	$("#PasswordConfirm").val('')
	$('#IdSexo').prop('selectedIndex', 0);
})
$(function () {
	startRefresh();
});
function startRefresh() {
	setTimeout(startRefresh, 1000);
	tblUsuarios.api().ajax.reload(null, false);
}
function DesactivaActiva(id) {
	$.ajax({
		url: '/Home/DesactivaActiva/' + id,
		type: 'GET',
		success: function (result) {

			msg("", result, "success");

		}
	});
}
function msg(titulo, mensaje, tipo) {
	$.notify({
		// options
		icon: 'fa fa-close',
		title: titulo,
		message: mensaje,

	}, {
			// settings
			element: 'body',
			position: null,
			type: tipo,
			allow_dismiss: true,
			newest_on_top: false,
			showProgressbar: false,
			placement: {
				from: "top",
				align: "center"
			},
			offset: 20,
			spacing: 10,
			z_index: 1031,
			delay: 5000,
			timer: 1000,
			mouse_over: null,
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			},
			onShow: null,
			onShown: null,
			onClose: null,
			onClosed: null,
			icon_type: 'class',
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
				'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
				'<span data-notify="icon"></span> ' +
				'<span data-notify="title">{1}</span> ' +
				'<span data-notify="message">{2}</span>' +
				'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				'</div>' +
				'</div>'
		});
}