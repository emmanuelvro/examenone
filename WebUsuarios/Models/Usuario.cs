﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUsuarios.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        [Required]
        public string User { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string PasswordConfirm { get; set; }
        [Required]
        public int IdSexo { get; set; }
        public string Sexo { get; set; }
        public bool Estatus { get; set; }
    }

    public enum Sexo
    {
        Masculino = 1,
        Femenino = 2
    }
}