﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUsuarios.Models
{
    public class LoginStatus
    {
        public bool Respuesta { get; set; }
        public string Mensaje { get; set; }
        public string Url { get; set; }
    }
}